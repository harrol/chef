package com.lissenberg.chef.frontend.bean;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Harro Lissenberg
 */
@Named
public class Menu {

    private String name = "Nieuw gerecht";
    private List<String> ingredients = new ArrayList<>();


    public Menu() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

}
