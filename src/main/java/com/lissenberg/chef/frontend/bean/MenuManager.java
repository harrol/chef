package com.lissenberg.chef.frontend.bean;

import com.lissenberg.chef.nevo.Main;
import com.lissenberg.chef.nevo.Product;
import com.ocpsoft.pretty.faces.annotation.URLMapping;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Harro Lissenberg
 */
@Named
@SessionScoped
@URLMapping(id = "menu", pattern = "/menu", viewId = "/menu.xhtml")
public class MenuManager implements Serializable {


    String ingredient;
    List<String> ingredients = new ArrayList<>();
    List<Product> products;

    @PostConstruct
    private void initialize() {
        products = Main.loadReader().getProducts();
    }

    private List<String> matchProducts(String product) {
        List<String> results = new ArrayList<>();
        for (Product p : products) {
            if (p.getDescription().toLowerCase().contains(product.toLowerCase())) {
                results.add(p.getDescription());
            }
        }
        return results;
    }

    public List<String> complete(String input) {
        return matchProducts(input);
    }

    public void addIngredient() {
        ingredients.add(ingredient);
        System.out.println(ingredients);
    }

    public void clearList() {
        ingredients.clear();
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public List<String> getIngredients() {
        return ingredients;
    }
}
