package com.lissenberg.chef.nevo;

/**
 * @author Harro Lissenberg
 */
public class Main {

    public static final String nutrients = "/Users/harro/projects/chef/nevo/Nevo-Nutrienten_Lijst_Online versie 2011_3.0.txt";
    public static final String nevo_dat = "/Users/harro/projects/chef/nevo/Nevo-Online versie 2011_3.0.dat";

    public static void main(String[] args) {
        NevoReader nevoReader = loadReader();
        System.out.println("Nutrients in database: " + nevoReader.getNutrientMap().size());
        System.out.println("Products in the database: " + nevoReader.getProducts().size());
    }

    public static NevoReader loadReader() {
        NevoReader nevoReader = new NevoReader();
        nevoReader.loadNutrients(nutrients);
        nevoReader.loadProducts(nevo_dat);
        return nevoReader;
    }
}
